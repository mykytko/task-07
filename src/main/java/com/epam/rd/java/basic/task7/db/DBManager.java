package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private Connection c = null;

	public static synchronized DBManager getInstance() {
		DBManager dbManager = new DBManager();
		try (FileReader reader = new FileReader("app.properties")) {
			Properties p = new Properties();
			p.load(reader);
			dbManager.c = DriverManager.getConnection(p.getProperty("connection.url"));
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
//		try {
//			Class.forName("org.postgresql.Driver");
//			dbManager.c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/test2db",
//					"postgres", "");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		instance = dbManager;
		return dbManager;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try {
			var preparedStatement = c.prepareStatement("select * from users");
			var results = preparedStatement.executeQuery();
			while (results.next()) {
				var user = new User();
				user.setId(results.getInt("id"));
				user.setLogin(results.getString("login"));
				users.add(user);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("insert into users(login) values(?)");
			preparedStatement.setString(1, user.getLogin());
			preparedStatement.executeUpdate();
			preparedStatement = c.prepareStatement("select * from users where login=?");
			preparedStatement.setString(1, user.getLogin());
			var res = preparedStatement.executeQuery();
			res.next();
			try {
				user.setId(res.getInt("id"));
			} catch (SQLException e) {
				throw new DBException(e.getMessage(), e);
			}
			preparedStatement.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try {
			boolean deleted = false;
			for (var user: users) {
				var preparedStatement =
						c.prepareStatement("delete from users where login=?");
				preparedStatement.setString(1, user.getLogin());
				if (preparedStatement.executeUpdate() != 0) {
					deleted = true;
				}
				preparedStatement.close();
			}
			return deleted;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public User getUser(String login) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("select * from users where login=?");
			preparedStatement.setString(1, login);
			var rs = preparedStatement.executeQuery();
			rs.next();
			var user = new User();
			try {
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				return null;
			}
			preparedStatement.close();
			return user;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("select * from teams where name=?");
			preparedStatement.setString(1, name);
			var rs = preparedStatement.executeQuery();
			rs.next();
			var team = new Team();
			try {
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			} catch (SQLException e) {
				throw new DBException(e.getMessage(), e);
			}
			preparedStatement.close();
			return team;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			var preparedStatement = c.prepareStatement("select * from teams");
			var results = preparedStatement.executeQuery();
			while (results.next()) {
				var team = new Team();
				team.setId(results.getInt("id"));
				team.setName(results.getString("name"));
				teams.add(team);
			}
			preparedStatement.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("insert into teams(name) values(?)");
			preparedStatement.setString(1, team.getName());
			preparedStatement.executeUpdate();
			preparedStatement = c.prepareStatement("select * from teams where name=?");
			preparedStatement.setString(1, team.getName());
			var res = preparedStatement.executeQuery();
			res.next();
			try {
				team.setId(res.getInt("id"));
			} catch (SQLException e) {
				throw new DBException(e.getMessage(), e);
			}
			preparedStatement.close();
			return true;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("select * from users_teams where user_id=?");
			preparedStatement.setInt(1, user.getId());
			var resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				for (var team: teams) {
					if (team.getId() == resultSet.getInt("team_id")) {
						throw new DBException("Duplicates are not allowed!", new SQLException());
					}
				}
			}
			c.setAutoCommit(false);
			var statement = c.createStatement();
			for (var team: teams) {
				statement.addBatch("insert into users_teams(user_id,team_id) " +
						"values(" + user.getId() + "," + team.getId() + ")");
			}
			var result = statement.executeBatch();
			boolean failed = false;
			int sum = 0;
			for (var res: result) {
				if (res < 0) {
					failed = true;
					break;
				}
				sum += res;
			}
			if (sum != teams.length) {
				failed = true;
			}
			if (failed) {
				c.rollback();
			} else {
				c.commit();
			}
			preparedStatement.close();
			c.setAutoCommit(true);
			return !failed;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			var preparedStatement =
					c.prepareStatement("select * from users_teams where user_id=" + user.getId());
			var results = preparedStatement.executeQuery();
			while (results.next()) {
				var team = new Team();
				team.setId(results.getInt("team_id"));
				teams.add(team);
			}
			for (var team: teams) {
				var innerStatement =
						c.prepareStatement("select * from teams where id=" + team.getId());
				var rs = innerStatement.executeQuery();
				rs.next();
				team.setName(rs.getString("name"));
			}
			preparedStatement.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("delete from teams where name=?");
			preparedStatement.setString(1, team.getName());
			boolean result = preparedStatement.executeUpdate() != 0;
			preparedStatement.close();
			return result;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			var preparedStatement =
					c.prepareStatement("update teams set name=? where id=?");
			preparedStatement.setString(1, team.getName());
			preparedStatement.setInt(2, team.getId());
			boolean result = preparedStatement.executeUpdate() != 0;
			preparedStatement.close();
			return result;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException(e.getMessage(), e);
		}
	}

}
